<?php namespace Germinate\Sitemap\Classes;

use Cms;
use File;
use Cms\Classes\Theme;

use ApplicationException;

/**
 * Automates / extends RainLab\Sitemap.
 *
 * @package germinate\sitemap
 * @author André Paterlini
 */
class Sitemap
{
    /**
     * Handler for the pages.menuitem.resolveItem event.
     * @param \RainLab\Sitemap\Classes\DefinitionItem $item Specifies the menu item.
     * @param string $url Specifies the current page URL, normalized, in lower case
     * The URL is specified relative to the website root, it includes the subdirectory name, if any.
     * @param \Cms\Classes\Theme $theme Specifies the current theme.
     * @param array $tree Specifies an array with items to be added to the sitemap.
     * @return mixed Returns an array. Returns null if the item cannot be resolved.
     */
    public static function resolveItem($item, string $url, Theme $theme, $tree)
    {
        $result = [];

        if ($item->nesting || count($tree) > 0) {
            $iterator = function($items) use (&$iterator, &$tree, $url) {
                $branch = [];

                foreach ($items as $itemInfo) {

                    // Skip hidden pages
                    if ($itemInfo['navigation_hidden']) {
                        continue;
                    }

                    $branchItem = [];
                    $branchItem['url'] = Cms::url($itemInfo['url']);

                    // Skip dynamic pages
                    if (str_contains($branchItem['url'], '/:')) {
                        continue;
                    }

                    $branchItem['isActive'] = self::urlsAreEqual($branchItem['url'], $url);
                    $branchItem['title'] = $itemInfo['title'];
                    $branchItem['mtime'] = $itemInfo['mtime'];

                    if ($itemInfo['items']) {
                        $branchItem['items'] = $iterator($itemInfo['items']);
                    }

                    $branch[] = $branchItem;
                }

                return $branch;
            };

            $result['items'] = $iterator( $tree );
        }

        return $result;
    }

    public static function getAllCmsPagesTree($theme)
    {
        $items = [];
        $pages = $theme->listPages();
        foreach($pages as $page) {
            $items[] = [
                'url'    => $page->url,
                'title'  => $page->title,
                'mtime'  => $page->mtime,
                'items'  => [],
                'parent' => null,
                'navigation_hidden' => $page->is_hidden
            ];
        }

        return $items;
    }

    public static function getAllStaticPagesTree($theme)
    {
        $items = [];
        $pageList = new \RainLab\Pages\Classes\PageList($theme);
        $pages = $pageList->listPages();
        foreach($pages as $page) {
            $items[] = [
                'url'    => $page->url,
                'title'  => $page->title,
                'mtime'  => $page->mtime,
                'items'  => [],
                'parent' => null,
                'navigation_hidden' => $page->is_hidden
            ];
        }

        return $items;
    }

    /**
     * Returns whether the specified URLs are equal.
     */
    protected static function urlsAreEqual($url, $other)
    {
        return rawurldecode($url) === rawurldecode($other);
    }

}