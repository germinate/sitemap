<?php namespace Germinate\Sitemap;

use Backend;
use Event;
use System\Classes\PluginBase;
use Germinate\Sitemap\Classes\Sitemap;

/**
 * Base Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = ['RainLab.Sitemap'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Sitemap',
            'description' => 'Sitemap plugin for Germinate projects',
            'author' => 'Germinate',
            'icon' => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boots (configures and registers) any packages found within this plugin's packages.load configuration value
     *
     * @see https://luketowers.ca/blog/how-to-use-laravel-packages-in-october-plugins
     * @author Luke Towers <octobercms@luketowers.ca>
     */
    public function bootPackages()
    {
        // Get the namespace of the current plugin to use in accessing the Config of the plugin
        $pluginNamespace = str_replace('\\', '.', strtolower(__NAMESPACE__));

        // Get the packages to boot
        $packages = Config::get($pluginNamespace . '::packages');

        // Boot each package
        $aliasLoader = AliasLoader::getInstance();
        foreach ($packages as $name => $options) {
            // Setup the configuration for the package, pulling from this plugin's config
            if (!empty($options['config']) && !empty($options['config_namespace'])) {
                Config::set($options['config_namespace'], $options['config']);
            }

            // Register any Service Providers for the package
            if (!empty($options['providers'])) {
                foreach ($options['providers'] as $provider) {
                    App::register($provider);
                }
            }
            
            // Register any Aliases for the package
            if (!empty($options['aliases'])) {
                foreach ($options['aliases'] as $alias => $path) {
                    $aliasLoader->alias($alias, $path);
                }
            }
        }
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // https://octobercms.com/docs/plugin/composer
        //$this->bootPackages();

        /*
         * 
         * Extend RainLab.sitemap
         * 
         */
        Event::listen('pages.menuitem.listTypes', function() {
            return [
                'all-cms-pages' => 'All CMS pages'
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
            if ( $type == 'all-cms-pages' ) {
                return [
                    'dynamicItems' => true
                ];
            }
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            if ( $type == 'all-cms-pages' ) {
                // Get all CMS pages
                $tree = Sitemap::getAllCmsPagesTree($theme);
                return Sitemap::resolveItem($item, $url, $theme, $tree);
            }
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Germinate\Base\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'germinate.base.some_permission' => [
                'tab' => 'Base',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'base' => [
                'label' => 'Base',
                'url' => Backend::url('germinate/base/mycontroller'),
                'icon' => 'icon-leaf',
                'permissions' => ['germinate.base.*'],
                'order' => 500,
            ],
        ];
    }

    /**
     * Registers extensions for Twig templates.
     *
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
        ];
    }

}
